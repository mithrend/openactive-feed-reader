<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Events extends Model
{
    
    protected $table = 'events';
    protected $fillable = [
        'feed',
        'feed_id',
        'title',
        'start_datetime',
        'thumbnail_url',
        'event_url',
        'location_id',
        'description'
    ];
    
    protected $appends = ['postcode'];

    public function location() {
        return $this->belongsTo('App\Location');
    }

    // change timezone to account for daylight saving time
    public function getUpdatedAtAttribute($timestamp) {
        $date = Carbon::parse($timestamp);
        $date->tz = 'Europe/London';
        return $date->format('d/m/y H:i');
    }
    
    // change timezone to account for daylight saving time
    public function getStartDatetimeAttribute($timestamp) {
        $date = Carbon::parse($timestamp);
        $date->tz = 'Europe/London';
        return $date->format('d/m/y H:i');
    }

    // always return the postcode for an event for mapping purposes
    public function getPostcodeAttribute() {
        if(!empty($this->location)) {
            return $this->location->postcode;
        }
    }

}
