<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Events as Events;
use App\Location as Location;

class Controller extends BaseController
{
    // Maps different feed api's to their url's
    // could have it's own db table to easily manage it in future
    protected $feedMap = [
        'Lets Ride' => 'http://api.letsride.co.uk/public/v1/rides',
        'British Triathlon' => 'https://api.britishtriathlon.org/openactive/v1/events'
    ];

    /**
     * Fetches live data for a feed and updates the db with the correct information
     *
     * @param String $feed
     */
    private function fetchAndProcess($feed) {
        // Get events data
        $eventsData = file_get_contents($this->feedMap[$feed]);

        // decode event data into an associative array
        $decodedEventsData = json_decode($eventsData, true);

        foreach($decodedEventsData['items'] as $event) {
            if(!isset($event['data'])) {
                continue;
            }
            $eventAddress = $event['data']['location']['address'];

            // Clean location data
            $nullableFields = [
                'streetAddress',
                'addressLocality',
                'addressRegion',
                'addressCountry',
            ];

            foreach($nullableFields as $nullableField) {
                if(!isset($eventAddress[$nullableField])) {
                    $eventAddress[$nullableField] = null;
                }
            }

            // Insert / Find location and return model
            $location = Location::firstOrCreate([
                'street' => $eventAddress['streetAddress'],
                'locality' => $eventAddress['addressLocality'],
                'region' => $eventAddress['addressRegion'],
                'country' => $eventAddress['addressCountry'],
                'postcode' => $eventAddress['postalCode']
            ]);
            
            // if the location is new, get lat and long data
            // don't do this for existing locations to save costs and time!
            if($location->wasRecentlyCreated || $location->lat == null || $location->lng == null) {
                $params = [
                    'key' => env('GOOGLE_API_KEY'),
                    'address' => trim($eventAddress['streetAddress'] . $eventAddress['addressLocality'] . $eventAddress['addressRegion'] . $eventAddress['postalCode'])
                ];
                $params = http_build_query($params);
                $result = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?' . $params);
                $decoded_result = json_decode($result, true);
                if(isset($decoded_result['results'][0]['geometry']['location'])) {
                    $location->lat = $decoded_result['results'][0]['geometry']['location']['lat'];
                    $location->lng = $decoded_result['results'][0]['geometry']['location']['lng'];
                    $location->save();
                }
            }
            
            // if description isn't set fallback to programme description if possible
            if(!isset($event['data']['description'])) {
                if(isset($event['data']['programme']['description']) && isset($event['data']['programme']['description'])) {
                    $event['data']['description'] = $event['data']['programme']['description'];
                } else {
                    $event['data']['description'] = null;
                }
            }
            
            $thumbnail = !isset($event['data']['programme']['logo']['url']) ? null : $event['data']['programme']['logo']['url'];

            // Insert / Update event
            Events::updateOrCreate([
                'feed' => $feed,
                'feed_id' => $event['id']
            ],[
                'title' => $event['data']['name'],
                'start_datetime' => $event['data']['startDate'],
                'thumbnail_url' => $thumbnail,
                'event_url' => $event['data']['url'],
                'description' => $event['data']['description'],
                'location_id' => $location->id
            ]);
        }
    }

    /**
     * Used for switching feeds from the client side
     * and returns data via json.
     * Will also get feed data if none yet exist in db.
     *
     * @param Request $request
     * @return json
     */
    public function getFeed(Request $request) {
        $feed = $request->input('feed', 'Lets Ride');
        $events = Events::where('feed', '=', $feed)->get();
        // if we have no data for this feed let's try and fetch some
        if($events->count() == 0) {
            fetchAndProcess($feed);
            $events = Events::where('feed', '=', $feed)->get();
        }
        
        $lastUpdated = Events::select('updated_at')->where('feed', '=', $feed)->orderBy('updated_at', 'desc')->first()->updated_at;
        $regions = Events::join('location', 'location.id', '=', 'events.location_id')
                           ->select('region')
                           ->where('feed','=', $feed)
                           ->groupBy('region')
                           ->get();
        
        return response()->json(['success' => true, 'lastUpdated' => $lastUpdated, 'data' => $events->toJson(), 'regions' => $regions->toJson()]);
    }

    /**
     * Takes a feed as a request param, updates the db with the latest data
     * and returns it to the user via json
     *
     * @param Request $request (feed)
     * @return json
     */
    public function getUpdate(Request $request) {
        // default to lets ride feed
        $feed = $request->input('feed', 'Lets Ride');
        $this->fetchAndProcess($feed);
        $lastUpdated = Events::select('updated_at')->where('feed', '=', $feed)->orderBy('updated_at', 'desc')->first()->updated_at;
        $events = Events::where('feed', '=', $feed)->get();
        return response()->json(['success' => true, 'lastUpdated' => $lastUpdated, 'data' => $events->toJson()]);
    }

    /**
     * Index function, used for initial page load
     *
     * @return view
     */
    public function index() {       
        $lastUpdated = Events::select('updated_at')->where('feed', '=', 'Lets Ride')->orderBy('updated_at', 'desc')->first()->updated_at;
        $events = Events::where('feed', '=', 'Lets Ride')->get();
        // if we have no data for this feed let's try and fetch some
        if($events->count() == 0) {
            fetchAndProcess('Lets Ride');
            $events = Events::where('feed', '=', 'Lets Ride')->get();
        }
        $regions = Events::join('location', 'location.id', '=', 'events.location_id')
                           ->select('region')
                           ->where('feed','=', 'Lets Ride')
                           ->groupBy('region')
                           ->get();


        return view('index', ['events' => $events, 'regions'=> $regions, 'lastUpdated' => $lastUpdated, 'endpoints' => array_keys($this->feedMap)]);
    }
}
