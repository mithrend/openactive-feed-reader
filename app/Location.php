<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';
    protected $fillable = [
        'street',
        'locality',
        'region',
        'country',
        'postcode'
    ];

    protected $appends = ['full_address'];

    public function events() {
        return $this->hasMany('App\Events');
    }

    public function getFullAddressAttribute() {
        return trim("{$this->street} {$this->locality} {$this->region} {$this->postcode}");
    }
}
