<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('street', 255)->nullable(true);
            $table->string('locality', 100)->nullable(true);
            $table->string('region', 100)->nullable(true);
            $table->string('country', 100)->nullable(true);
            $table->string('postcode', 8);
            $table->decimal('lat',12,4)->nullable(true);
            $table->decimal('lng',12,4)->nullable(true);
            $table->unique(['street', 'locality', 'region', 'country', 'postcode']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
