<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('feed')->index(); // What feed we are using, i.e letsride
            $table->integer('feed_id'); // The id we recieve from the feed
            $table->string('title', 255);
            $table->dateTimeTz('start_datetime');
            $table->text('thumbnail_url')->nullable(true);
            $table->text('event_url')->nullable(true);
            $table->text('description')->nullable(true);
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on('location')->onDelete('cascade')->onUpdate('cascade'); // Use location table as multiple events could have same location
            $table->unique(['feed', 'feed_id']); // Make these unique so we can easily update events later
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
