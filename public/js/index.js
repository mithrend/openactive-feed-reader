var filtered = {
    search: false,
    region: false
};
var filteredData = {
    search: [],
    region: []
};

var gMap;
var gMapMarkers = [];
$(function() {
    $('#map_view_btn').on('click', function() {
        $('#list_view').hide(0);
        $('#map_view').show(0);
    });

    $('#list_view_btn').on('click', function() {
        $('#map_view').hide(0);
        $('#list_view').show(0);

    });

    $('#refresh_btn').on('click', function() {
        updateData();
    });

    $('#region_filter').on('change', function() {
        filterByRegion();
    });

    $('#search_filter').on('change', function() {
        filterBySearch();
    });

    $('#feed_selector').on('change', function() {
        changeFeed();
    });

    initMap();
});

function addMapMarkers(resultsMap, data) {
    $.each(data, function(i,v) {
        // only map location we have geocode data for!
        if(v.location.lat != null && v.location.lng != null) {
            // construct content for marker popup
            var markerContent = '<div><h5 class="d-inline-block">'+v.title+'</h5>';
            if(v.thumbnail_url != null) {
                markerContent += '<img class="event-thumb float-right" src="'+v.thumbnail_url+'" alt="event thumbnail"/>'
            }
            markerContent += '<div class="mb-1"><strong>Location:</strong> '+v.location.full_address + '</div>'
                + '<div class="mb-1"><strong>Date:</strong> '+v.start_datetime + '</div>'
                + '<div class="mb-1"><strong>Description:</strong> '+v.description + '</div>';

            if(v.event_url != null) {
                markerContent += '<div><a href="'+v.event_url+'" >Find out more here</a></div>'
            }

            markerContent += '</div>';

            var infoWindow = new google.maps.InfoWindow({
                content: markerContent
            });

            var marker = new google.maps.Marker({
                map: resultsMap,
                position: {lat: parseFloat(v.location.lat), lng: parseFloat(v.location.lng)}
            });

            gMapMarkers.push(marker);

            marker.addListener('click', function() {
                infoWindow.open(resultsMap, marker);
            });
        }
    });

    // this method makes the map visible so this just hides it quickly again
    if($('#list_view_btn').hasClass('active')){
        $('#map_view').hide(0);
    }
}

function changeFeed() {
    var refreshBtn = $('#refresh_btn');
    refreshBtn.prop('disabled', true);
    refreshBtn.find('i').addClass('rotate');
    var feedSelector = $('#feed_selector');
    var feed = feedSelector.val();
    feedSelector.prop('disabled', true);
    var encodedFeed = encodeURI(feed);
    $.ajax({
        url: 'get_feed?feed=' + encodedFeed,
        type: 'get',
        dataType: 'json',
        success: function(json) {
            if(json.success) {
                data = JSON.parse(json.data);
                $('#search_filter').val('');
                $('#region_filter').val('');
                filtered.region = false;
                filtered.search = false;
                var regionHtml = '<option value="">All Regions</option>';
                $.each(JSON.parse(json.regions), function(i,v) {
                    var regionVal = v.region == null ? 'Unknown' : v.region;
                    regionHtml += '<option calue="'+regionVal+'">'+regionVal+'</option>';
                });
                $('#region_filter').html(regionHtml);
                redrawCombinedFilters();
                $('#last_updated').html(json.lastUpdated);
                showMsg('Feed successfully fetched');
            } else {
                showMsg('Oops, Something went wrong!', 'error');
            }
            
            feedSelector.prop('disabled', false);
            refreshBtn.prop('disabled', false);
            refreshBtn.find('i').removeClass('rotate');
        }
    });
}

function clearMapMarkers() {
    for(i=0; i<gMapMarkers.length; i++) {
        gMapMarkers[i].setMap(null);
    }
}

function filterByRegion() {
    // disable search filter
    $('#search_filter').prop('disabled', true);
    var filterRegion = $('#region_filter').val();
    // if we have no region we have to unfilter
    if(filterRegion !== "") {
        // we don't get some regions from the api!
        if(filterRegion == 'Unknown') {
            filterRegion = null;
        }

        filteredData.region = data.filter(function(event) {
            return event.location.region == filterRegion;
        });

        filtered.region = true;
    } else {
        filteredData.region = [];
        filtered.region = false;
    }

    redrawCombinedFilters();
}

    function filterBySearch() {
    // disable region filter
    $('#region_filter').prop('disabled', true);

    var searchTerm = $('#search_filter').val();

    // if we have no search term we have to unfilter
    if(searchTerm !== '') {
        // Do a case insensitive search
        var searchTermReg = new RegExp(searchTerm,"i");
        filteredData.search = data.filter(function(event) {
            var matchedTitle = event.title.search(searchTermReg) === -1 ? false : true;
            var matchedDesc = event.description.search(searchTermReg) === -1 ? false : true;
            var matchedAddr = event.location.full_address.search(searchTermReg) === -1 ? false : true;
            if(matchedTitle || matchedDesc || matchedAddr) {
                return true;
            }
        });

        filtered.search = true;
    } else {
        filteredData.search = [];
        filtered.search = false;
    }

    redrawCombinedFilters();
}

    function initMap() {
    gMap = new google.maps.Map(document.getElementById('map_view'), {
        zoom: 6,
        center: {lat: 54.00, lng: -3.00}
    });

    addMapMarkers(gMap, data);
}

    function redrawCombinedFilters() {
    var dataToDraw = [];
    if(filtered.region && filtered.search) {
        var mappedRegion = filteredData.region.map(function(event){ return event.id });
        var mappedSearch = filteredData.search.map(function(event){ return event.id });
        var idsToDraw = mappedRegion.filter(function(value){ return -1 !== mappedSearch.indexOf(value) });
        dataToDraw = data.filter(function(value){ return -1 !== idsToDraw.indexOf(value.id)})
    } else if (filtered.region) {
        dataToDraw = filteredData.region;
    } else if (filtered.search) {
        dataToDraw = filteredData.search;
    } else {
        dataToDraw = data;
    }

    redrawData(dataToDraw);
    // enable filters
    $('#region_filter').prop('disabled', false);
    $('#search_filter').prop('disabled', false);
}

    function redrawData(data) {
    refreshTable(data);
    clearMapMarkers();
    addMapMarkers(gMap, data);
}

    function refreshTable(data) {
    // construct table html in string
    var tbody = '';
    $.each(data, function(i,v) {
        tbody += '<tr>';
        if(v.thumbnail_url != null) {
            tbody += '<td><img class="event-thumb" src="' + v.thumbnail_url + '" alt="event thumbnail" /></td>';
        } else {
            tbody += '<td></td>';
        }
            tbody += '<td data-label="Title">' + v.title + '</td>';
            tbody += '<td data-label="Desc"><div class="event-desc">' + v.description + '</div></td>';
            tbody += '<td data-label="Date">' + v.start_datetime + '</td>';
            tbody += '<td data-label="Loc">' + v.location.full_address + '</td>';
            tbody += '<td><a href="' + v.event_url + '"><i class="material-icons">link</i></a></td>';
        tbody += '</tr>';
    });

    $('#list_view tbody').html(tbody);
}

// shows a message to the user at top of screen
// defaults to success type
function showMsg(message, type) {
    if(typeof type == 'undefined') {
        var type = 'success';
    }

    var alert = '<div class="alert alert-'+type+' alert-dismissible fade show" role="alert">'
        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        +    '<span aria-hidden="true">&times;</span>'
        +  '</button>'+message+'</div>';

    $('body').prepend(alert);
}

function updateData() {
    // disable refresh button and animate icon to show loading
    var refreshBtn = $('#refresh_btn');
    refreshBtn.prop('disabled', true);
    refreshBtn.find('i').addClass('rotate');
    var feed = $('#feed_selector').val();
    var encodedFeed = encodeURI(feed);
    $.ajax({
        url: 'get_update?feed=' + encodedFeed,
        type: 'get',
        dataType: 'json',
        success: function(json) {
            if(json.success) {
                data = JSON.parse(json.data);
                if(filtered.region) {
                    filterByRegion();
                }
                if(filtered.search) {
                    filterBySearch();
                }
                redrawCombinedFilters();
                $('#last_updated').html(json.lastUpdated);
                showMsg('Data successfully refreshed');
            } else {
                showMsg('Oops, Something went wrong!', 'error');
            }
            
            refreshBtn.prop('disabled', false);
            refreshBtn.find('i').removeClass('rotate');
        }
    });
}