@extends('layout')

@section('title', 'OpenActive Data')

@section('stylesheets')
<link rel="stylesheet" href="{{ URL::asset('css/index.min.css') }}">
@endsection

@section('content')
    <h1 class="text-center mb-3 mt-1">OpenActive Data</h1>
    <div class="row justify-content-center mb-3 mt-1">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-primary active" id="list_view_btn">
                <input type="radio" name="list" autocomplete="off" checked> List View
            </label>
            <label class="btn btn-primary" id="map_view_btn">
                <input type="radio" name="map" autocomplete="off"> Map View
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Feed</div>
                </div>
                <select id="feed_selector" class="custom-select">
                    @foreach($endpoints as $endpoint)
                        <option {{ $endpoint == 'Lets Ride' ? 'selected' : '' }} value="{{ $endpoint }}">{{ $endpoint }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="input-group">
            <div class="col-md-9 mb-2">
            <input id="search_filter" class="form-control" placeholder="Type to search..." />
            </div>
            <div class="col-md-3 mb-2">
            <select id="region_filter" class="custom-select">
                    <option value="" selected>All Regions</option>
                    @foreach($regions as $region)
                        @if($region->region == null)
                            <option value="Unknown">Unknown</option>
                            @continue
                        @endif
                        <option value="{{ $region->region }}">{{ ucfirst($region->region) }}</option>   
                    @endforeach
            </select>
            </div>
        </div>
    </div>
    <div>
        <button id="refresh_btn" type="button" class="btn btn-light mb-1" title="refresh data" ><i class="material-icons">refresh</i></a></button>
        <span class="float-right mt-1">Last Updated: <span id="last_updated">{{ $lastUpdated }}</span></span>
    </div>
    <div id="list_view" class="table-responsive-sm">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td></td>
                    <td>Title</td>
                    <td>Description</td>
                    <td>Date</td>
                    <td>Location</td>
                    <td>Link</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($events as $event)
                    <tr>
                        <td data-label=""><img class="event-thumb" src="{{ $event->thumbnail_url }}" alt="event thumbnail" /></td>
                        <td data-label="Title">{{ $event->title }}</td>
                        <td data-label="Desc"><div class="event-desc">{!! $event->description !!}</div></td>
                        <td data-label="Date">{{ $event->start_datetime }}</td>
                        <td data-label="Loc">{{ $event->location->fullAddress }}</td>
                        <td data-label=""><a href="{{ $event->event_url }}"><i class="material-icons">link</i></a></td>
                    </tr>
                @endforeach
        </tbody>
        </table>
    </div>
    <div id="map_view">

    </div>
@endsection

@section('script')
    <script>
        var data = <?php echo $events->toJson()?>;
    </script>
    <script src="{{URL::asset('js/index.min.js')}}"></script>
@endsection